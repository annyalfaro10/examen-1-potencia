syms H
N=400;
I=1;
L1=0.5;
L2=0.15;
L3=0.49999;
L4=0.01E-3;
m=1.256637061E-6;
h0=(L1*N*I)/(L3*(2*L2+L1));
b0=(L1*m*N*I)/(L4*(2*L2+L1));

B1(H)=(2*H)/(400+H);
B2(H)=b0-H*((b0)/(h0));
a=0;
b=h0;

tol=[0.01, 0.001, 0.0001, 0.00001, 0.000001];

contador=1;

met_vec_bt=zeros(1,5);
met_vec_ht=zeros(1,5);
num_tolerancia=1;
tic
for tolerancia=tol
    
    t=round((b)/(tolerancia));
    v=zeros(1,t);
    h=a:tolerancia:b;
    
    for i = a:tolerancia:b
        v(contador)=abs(B1(i)-B2(i));
        contador=contador+1;
    end

    [M,I]=min(v);

    Bt=(B2(h(I))+B1(h(I)))/(2);
    Ht=h(I);
    toc
    met_vec_bt(1,num_tolerancia)=Bt;
    met_vec_ht(1,num_tolerancia)=Ht;
    num_tolerancia=num_tolerancia+1;

    disp(double(Bt))
    disp(Ht)

end


