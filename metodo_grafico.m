syms H;
b0 = 31.41592654;
h0 = 500.10002;
met_grafico = zeros(2,50011);
B1 = (2*H)/(400+H);
B2 = b0 - ((b0*H)/h0);
contador = 1;
for h = 0:0.01:500.10002
    b2 = eval(subs(B2,H,h));
    b1 = eval(subs(B1,H,h));
    met_grafico(1, contador) = b1;
    met_grafico(2, contador) = b2;
    contador = contador+1;
end

h = 0:0.01:500.10002;
plot(h,met_grafico(1,:))
hold on
plot(h,met_grafico(2,:))
hold off