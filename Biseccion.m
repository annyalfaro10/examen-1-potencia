syms H
N=400;
I=1;
L1=0.5;
L2=0.15;
L3=0.49999;
L4=0.01E-3;
m=1.256637061E-6;
h0=(L1*N*I)/(L3*(2*L2+L1));
b0=(L1*m*N*I)/(L4*(2*L2+L1));


B1(H)=(2*H)/(400+H);
B2(H)=b0-H*((b0)/(h0));
a=0;
b=h0;
%tol=0.0001;
dh=0.001;
tic
tol = [0.01, 0.001, 0.0001, 0.00001, 0.000001];
num_tolerancia = 1;

for tolerancia = tol
    for i=a:dh:b
        c=(a+b)/2;
        v= abs(B1(c)-B2(c));

        if v<=tolerancia
            Bt=(B2(c)+B1(c))/2;
            Ht=c;
            break
        end  
        q=sign(B2(a)-B1(a));
        p=sign(B2(c)-B1(c));
        if p==q
            a=c;
        else
            b=c;
        end

    end
toc
disp(tolerancia)
disp(double(Bt))
num_tolerancia = num_tolerancia + 1;
end