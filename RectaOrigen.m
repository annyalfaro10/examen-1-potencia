syms H
syms x
N=400;
I=1;
L1=0.5;
L2=0.15;
L3=0.49999;
L4=0.01E-3;
m=1.256637061E-6;
h0=(L1*N*I)/(L3*(2*L2+L1));
b0=(L1*m*N*I)/(L4*(2*L2+L1));

tol = [0.01, 0.001, 0.0001, 0.00001, 0.000001];


B1(H)=b0-H*((b0)/(h0));
B2(H)=(2*H)/(400+H);


dh=0.001;
hn=0;
Pa=([0,0]);

for tolerancia = tol
    tic
   
    for i=0:dh:h0
        
        P=([hn+tolerancia, B2(hn+tolerancia)]);
        ys=(P(2))/(P(1));
        hn=(b0)/(ys+(b0)/(h0));
        disp(double(hn))
        
        v= abs(B1(hn)-B2(hn));

        if v<=tolerancia
           Bt=(B2(hn)+B1(hn))/2;
           Ht=hn;
           break
  
        end
    end
toc
disp(tolerancia)
disp(double(Bt))
end
