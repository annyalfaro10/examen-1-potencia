syms H;
b0 = 31.41592654;
h0 = 500.10002;
tol = [0.01, 0.001, 0.0001, 0.00001, 0.000001];
met_iter = zeros(5,50011);
B1 = (2*H)/(400+H);
B2 = b0 - ((b0*H)/h0);
met_iter_bt = zeros(1,5);
met_iter_ht = zeros(1,5);
num_tolerancia = 1;
for tolerancia = tol
    contador = 1;
    tic
    for h = 0:0.01:500.10002
        b2 = eval(subs(B2,H,h));
        b1 = eval(subs(B1,H,h));
        resta = abs(b2 - b1);
        met_iter(num_tolerancia, contador) = resta;
        contador = contador + 1;
        if resta <= tolerancia
            met_iter_bt(1,num_tolerancia) = (b2+b1)/2;
            met_iter_ht(1,num_tolerancia) = h;
            break
        end   
    end
    toc
    num_tolerancia = num_tolerancia + 1;
end